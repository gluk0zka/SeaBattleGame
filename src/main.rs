slint::slint!{
	component  MemCube inherits Rectangle{
		width: 64px;
		height: 64px;
		background: #3960D5;
	}
    export component MainWindow inherits Window{
		MemCube {}
	}
}
 
fn main() {
    MainWindow::new().unwrap().run().unwrap();
}